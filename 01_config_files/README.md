# Generate Config files

When managing a lot of servers, automatically generating config files is really convenient.

For instance, consider that a certain configuration file needs to be present on thousands of servers. [ansible](https://docs.ansible.com/ansible/latest/index.html) is a configuration tool that can do just that.

Under the hood, it uses jinja templates to render configuration files. In this folder, we will examine a basic example.


## Variables

When rendering a Template, variables are denoted by double curly braces:

```
This is my ip address: {{ personal_ip_adress }}
```

## Rendering

The data is supplied in a python dict:

```py
data = {
    "personal_ip_address": 123.123.23.1,
    ...
}
```