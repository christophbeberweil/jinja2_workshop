from jinja2 import Environment, FileSystemLoader, select_autoescape


env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape()
)


template = env.get_template("ssh_config.j2")

rendered_content = template.render(
    data={
        "user": "bob",
        "ip_address": "111.111.11.11",
        "port": 22
    },
)

print(rendered_content)

textfile = open("ssh_config.txt", "w")
a = textfile.write(rendered_content)
textfile.close()

