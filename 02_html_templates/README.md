# HTML Files

Frameworks such as [Django](https://www.djangoproject.com/) use jinja in order to render data from a database to html. This is a basic component in building web applications.

For instance, a website could consider the users username along with some further information:

```html
<div>
    <p>Username: {{ username }}</p>
    <p>Last login: {{ last_login }}</p>
</div>
```

```python
data = {
    "username": "olaf",
    "last login": "2022-01-01"
}
```
