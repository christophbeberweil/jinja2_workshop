from jinja2 import Environment, FileSystemLoader, select_autoescape


env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape()
)


template = env.get_template("account_information.html")

rendered_content = template.render(
    data={
        "username": "olaf",
        "last_login": "2022-01-01"
    },
)

print(rendered_content)

textfile = open("account_information.html", "w")
a = textfile.write(rendered_content)
textfile.close()