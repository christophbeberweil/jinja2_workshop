# If statements



Jinja supports if statements:

```
{% if condition %}
    Text when true
{% else %}
    Text when false
{% endif %}
```

## Example 

```html
{% if order.completed %}
    <p>Thank you, your order has been completed.</p>
{% else %}
    <button>Order now!</button>
{% endif %}
```

## Else if

Display the type of a ticket in a ticketsystem with a fallback if there is no type.

```html
{% if ticket.type == 'bug'%}
    <div style="background-color: red;">Bug</div>
{% elif ticket.type == 'feature' %}
    <div style="background-color: green;">Feature request</div>
{% elif ticket.type == 'support_request' %}
    <div style="background-color: blue;">Customer support request</div>
{% else %}
    <div style="background-color: yellow;">Unknown</div>
{% endif %}
```
