# For Loops

```html
<ul>
    {% for entry in users_list %}
        <li> {{entry.username}} </li>
    {% endfor %}
</ul>
```

Data:
```python
data = [
    {
        "username": "alice",
        "last_login": None,
    },
    {
        "username": "bob",
        "last_login": "2021-06-12",
    },
    {
        "username": "charlie",
        "last_login": "2022-08-05",
    },
]
```
Result:

```html
<ul>
    <li>Alice</li>
    <li>Bob</li>
    <li>Charlie</li>
</ul>
```
