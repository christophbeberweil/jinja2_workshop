# Filters

https://jinja.palletsprojects.com/en/3.1.x/templates/#list-of-builtin-filters

Filters can format a variable before inserting it into the template

Data:

```python
data = {
    "username": "alice",
    "last_login": datetime.now(),
    "interests": [
        {
            "id": "7a89c444-9432-42f7-ba2f-3a46bc5d76e9",
            "name": "Accelerator Physics",
        },
        {
            "id": "7edd2384-86c9-46ab-b1a9-78469645cb29",
            "name": "Neurobiology",
        },
    ]
}
```

## Some examples using the data above:

```html
<div>{{username|capitalize}}</div>
```

```html
<div>Alice</div>
```

---

Filters can be chained

```html
<div>Interests: {{ interests|map(attribute='name')|join(', ') }}</div>
```

```html
<div>Interests: Accelerator Physics, Neurobiology</div>
```

---

You can write python insie the curly braces

```html
<div>Last Login: {{ last_login.strftime('%d.%m.%y') }}</div>
```

```html
<div>Last Login: 31.12.2021</div>
```

---
