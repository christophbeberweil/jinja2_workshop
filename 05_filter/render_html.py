from jinja2 import Environment, FileSystemLoader, select_autoescape

from datetime import datetime

env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape()
)


template = env.get_template("profile_page.html")

data = {
        "username": "alice",
        "last_login": datetime.now(),
        "tags": ["tag1", "tag2", "tag3"],
        "is_admin": False,
        "interests": [
            {
                "id": "7a89c444-9432-42f7-ba2f-3a46bc5d76e9",
                "name": "Accelerator Physics",
            },
            {
                "id": "7edd2384-86c9-46ab-b1a9-78469645cb29",
                "name": "Neurobiology",
            },
        ]
    }
rendered_content = template.render(
    data=data
)

print(rendered_content)

textfile = open("profile_page.html", "w")
a = textfile.write(rendered_content)
textfile.close()