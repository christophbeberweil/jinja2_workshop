# Templating with Jinja 2



Christoph Beberweil
Screenion GmbH

- What is templating?
- Why is it useful?
- How is it done?

---
## What is templating?

Transform a template (textfile with variables in it) into 
a textfile (variables replaced with useful content).



--- 

## Why is it useful?

- Generate config files (ansible)
- Render data to HTML (Django Web-Apps)
- ...


---

### Example

Data:
```
{
  "group": "NNP AG",
  "project": "F8SR",
  "members": [
     "Julie",
     "Olaf",
     "Anne",
     "Tom",
   ],
}
```


---
### Example

Template:
```html
<body>

  <h1>{{group}}</h2>
  
  <h2>{{project}}

  <ul>
   {% for member in members %}
   <li>{{member}}</li>
   {% endfor %}
  </ul>

</body>
```
---

### Example

Generated HTML:
```html
<body>

  <h1>NNP AG</h1>

  <h2>F8SR</h2>

  <ul>
   <li>Julie</li>
   <li>Olaf</li>
   <li>Anne</li>
   <li>Tom</li>
  </ul>

</body>
```


---
<!-- effect=stars -->
# Onwards to the practical part!



---
<!-- effect=fireworks -->
# Thank you!


